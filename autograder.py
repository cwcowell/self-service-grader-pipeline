import os
import requests
import sys
import urllib.parse

# 150/150 in legacy env
# PROJECT_URL_UNENCODED: str = 'https://gitlab-core.us.gitlabdemo.cloud/training-users/session-60586cb7/iuten15m/gitlab-certified-assoc'

# 150/150 in spt env
# PROJECT_URL_UNENCODED = 'https://spt.gitlabtraining.cloud/iumhukdg-group/gitlab-certified-assoc'

# my project in ilt env
# PROJECT_URL_UNENCODED = 'https://ilt.gitlabtraining.cloud/training-users/session-822385f1/my-test-group-abcde/gitlab-certified-assoc'

PROJECT_URL_UNENCODED = os.environ['PROJECT_URL']

print(f"Grading the Certified Associate hands-on assessment at '{PROJECT_URL_UNENCODED}'")


def urlencode(unencoded_string: str) -> str:
    """Convert a non-urlencoded string into a urlencoded String.

    For example: "foo/bar" becomes "foo%2Fbar"
    :return: urlencoded version of the unencoded string
    """
    return urllib.parse.quote(unencoded_string, safe='')


def urlunencode(encoded_string: str) -> str:
    """Convert a urlencoded string into a non-urlencoded String.

    For example: "foo%2Fbar" becomes "foo/bar"
    :return: non-urlencoded version of the encoded string
    """
    return urllib.parse.unquote(encoded_string)


def extract_group_path(project_path: str) -> str:
    """Extract the group path from the longer project path. Both are urlencoded.

    :param project_path: urlencoded project path
    :return: urlencoded group path
    """
    path_separator: str = urllib.parse.quote('/', safe='')
    group_path_project_idx: int = project_path.rfind(path_separator)
    group_path: str = project_path[:group_path_project_idx]
    return group_path


def get_personal_access_token() -> str:
    """Retrieve a personal access token from the appropriate env var.

    :return: personal access token for the appropriate env
    """
    env: str = get_env()

    if env == 'legacy':
        personal_access_token: str = os.environ.get('PERSONAL_ACCESS_TOKEN_FOR_LEGACY')
    elif env == 'spt':
        # personal_access_token: str = os.environ.get('PERSONAL_ACCESS_TOKEN_FOR_SPT')
        personal_access_token: str = 'DPUdfyQxFwF2Zjon2ss2'
    elif env == 'ilt':
        personal_access_token: str = os.environ.get('PERSONAL_ACCESS_TOKEN_FOR_ILT')
    else:
        sys.exit(f"ERROR: unrecognized environment: '{env}'")

    if (personal_access_token is None) or (personal_access_token == ''):
        sys.exit(f"ERROR: the PERSONAL_ACCESS_TOKEN_FOR_{env.upper()} environment variable is not set. "
                 f"See the autograder README.")
    personal_access_token = personal_access_token.strip()
    return personal_access_token


def create_headers() -> dict:
    """Return standard headers to be used with every REST API call.

    :return: accept content type and token
    """
    return {
        'Accept': 'application/json',
        'PRIVATE-TOKEN': get_personal_access_token()
    }


def call_api(url: str) -> (object, int):
    """Call a REST API endpoint.

    :param url: the full URL of the REST API call, including any parameters
    :return: response object and status code
    """
    response = requests.get(url, headers=create_headers())
    code = response.status_code
    if code == 401:
        sys.exit("ERROR: Demo Cloud won't authenticate with that personal access token.")
    return response, code


def find_issues_with_title_and_label_under_epic(base_api_url: str,
                                                epic_title_unencoded: str,
                                                group_path_encoded: str,
                                                expected_issue_title: str,
                                                expected_issue_label: str) -> list:
    # Get all epics within the group
    epic_title_encoded: str = urlencode(epic_title_unencoded)
    group_path_unencoded = urlunencode(group_path_encoded)
    url: str = f'{base_api_url}/groups/{group_path_encoded}/epics?search={epic_title_encoded}&in=title'
    epics_response, epics_code = call_api(url)
    epics = epics_response.json()
    # If it finds no epics within the group, it returns a dict containing an error message.
    # If it does finds epics within the group, it returns a list of epics, each represented as a dict.
    assert (type(epics) is not dict), f"didn't find any epics in the group at '{group_path_unencoded}'"

    issues_with_right_title_and_label: list = []  # there could be multiple issues that match
    for epic in epics:
        # Check all issues attached to this epic to see if any have the right title and label
        epic_iid: str = epic['iid']
        url: str = f'{base_api_url}/groups/{group_path_encoded}/epics/{epic_iid}/issues'
        issues_response, issues_code = call_api(url)
        if issues_code == 404:
            continue  # this epic has no issues, so go on to the next epic
        issues: list = issues_response.json()
        for issue in issues:
            if (issue['title'] == expected_issue_title) and (expected_issue_label in issue['labels']):
                issues_with_right_title_and_label.append(issue)
    return issues_with_right_title_and_label


def get_commits_on_branch(base_api_url: str, branch_unencoded: str, project_path_encoded: str) -> list:
    """Return commits on this branch, most recent first.

    :param base_api_url: the URL to build API calls off of by adding a path
    :param branch_unencoded: the branch to fetch commits from
    :param project_path_encoded: the project that the branch lives in
    :return: the branch's commits, most recent first
    """
    branch_encoded: str = urlencode(branch_unencoded)
    project_path_unencoded: str = urlunencode(project_path_encoded)
    if branch_unencoded == 'default':
        url: str = f'{base_api_url}/projects/{project_path_encoded}/repository/commits'
    else:
        url: str = f'{base_api_url}/projects/{project_path_encoded}/repository/commits?ref_name={branch_encoded}'
    response, code = call_api(url)
    commits = response.json()
    assert(len(commits) > 0), f"didn't find any commits on the branch called '{branch_unencoded}' " \
                              f"in the project at '{project_path_unencoded}'"
    return commits


def get_diffs_from_commit(base_api_url: str, project_path_encoded: str, sha: str) -> list:
    """Return all the diffs made by a single commit."""
    url: str = f'{base_api_url}/projects/{project_path_encoded}/repository/commits/{sha}/diff'
    response, code = call_api(url)
    diffs: list = response.json()
    return diffs


def extract_protocol_from_url(url: str) -> str:
    """Return just the protocol of a URL.

    For example, if passed ``https://www.foo.com/bar``, return ``https://``
    """
    for protocol_candidate in ['https://', 'http://']:
        if url.startswith(protocol_candidate):
            return protocol_candidate
        else:
            return ''


def extract_subdomain_and_domain_from_url(url: str, protocol: str) -> str:
    """Return just the subdomain and domain of a URL.

    For example, if passed ``https://www.foo.com/bar`` return ``www.foo.com``
    """
    first_char_of_path: int = url.find('/', len(protocol))
    if first_char_of_path == -1:
        # there are no slashes after the domain, so domain is everything after the protocol
        return url[len(protocol):]
    else:
        # there is at least one slash after the domain, so domain is everything between the protocol and the slash
        return url[len(protocol):first_char_of_path]


def extract_protocol_and_subdomain_and_domain_from_url(url: str) -> str:
    """Return just the protocol, subdomain, and domain of a URL.

    For example, if passed ``https://www.foo.com/bar``, return ``https://www.foo.com``,
    where ``https://`` is the protocol, ``www`` is the subdomain, and ``foo.com`` is the domain.
    """
    protocol: str = extract_protocol_from_url(url)
    subdomain_and_domain: str = extract_subdomain_and_domain_from_url(url, protocol)
    return protocol + subdomain_and_domain


def extract_path_from_url(url: str) -> str:
    """Return just the path of a URL.

    For example, if passed ``https://www.foo.com/bar/baz``, return ``/bar/baz``
    """
    protocol_and_subdomain_and_domain: str = extract_protocol_and_subdomain_and_domain_from_url(url)
    return url[len(protocol_and_subdomain_and_domain):]


def get_env() -> str:
    """Inspect the project's URL to figure out what environment the work was done in.

    :returns: the environment, which will be one of 'legacy', 'ilt', or 'spt'
    """
    if 'gitlab-core.us.gitlabdemo.cloud' in PROJECT_URL_UNENCODED:
        return 'legacy'
    elif 'ilt.gitlabtraining.cloud' in PROJECT_URL_UNENCODED:
        return 'ilt'
    elif 'spt.gitlabtraining.cloud' in PROJECT_URL_UNENCODED:
        return 'spt'
    else:
        sys.exit(f"Unrecognized environment: '{PROJECT_URL_UNENCODED}'")


def get_urls():
    """Set base URLs depending on what env this assessment was created in."""
    base_env_url = extract_protocol_and_subdomain_and_domain_from_url(PROJECT_URL_UNENCODED)
    base_api_url = f'{base_env_url}/api/v4'
    project_path_unencoded: str = extract_path_from_url(PROJECT_URL_UNENCODED)
    project_path_unencoded = project_path_unencoded[1:]  # remove the initial slash
    project_path_encoded: str = urlencode(project_path_unencoded)
    return base_api_url, project_path_unencoded, project_path_encoded


def branch_exists(branch_unencoded: str) -> bool:
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    branch_encoded = urlencode(branch_unencoded)
    url: str = f'{base_api_url}/projects/{project_path_encoded}/repository/branches/{branch_encoded}'
    response, code = call_api(url)
    return code == 200


# ---
# tests
# ---

def test_task_1() -> None:
    """
    Navigate to GROUPS > YOUR GROUPS and create a project titled "GitLab Certified Assoc"
    based on the NodeJS Express template.
    """
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()

    # Does the project exist?
    url: str = f'{base_api_url}/projects/{project_path_encoded}'
    response, code = call_api(url)
    assert (code == 200), f"no project exists at '{project_path_unencoded}'"

    # Does the project have the right name?
    response_contents: dict = response.json()
    expected_project_title: str = 'GitLab Certified Assoc'
    assert (response_contents['name'] == expected_project_title), \
        f"the project at {project_path_unencoded} isn't titled '{expected_project_title}'"

    # Does the project use the right template?
    required_template_file_path: str = '.node-version'
    file_path_encoded: str = urlencode(required_template_file_path)
    found_file: bool = False
    branches: list = ['master', 'main']
    for branch in branches:
        url: str = f'{base_api_url}/projects/{project_path_encoded}/repository/files/{file_path_encoded}?ref={branch}'
        response, code = call_api(url)
        if code == 200:
            found_file = True
            break
    assert found_file, f"the project at '{project_path_unencoded}' wasn't created from the NodeJS Express template"


def test_task_2() -> None:
    """
    On a new branch called "branch-update-readme", commit a change to your README that adds a description
    of your project.

    1. Get commits on the 'branch-update-readme' branch
    2. Iterate through commits, looking for a change to README.md
    """
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    expected_branch_unencoded: str = 'branch-update-readme'
    expected_branch_encoded: str = urlencode(expected_branch_unencoded)
    expected_edited_file = 'README.md'

    if branch_exists(expected_branch_unencoded):
        # get all commits for our branch and for the default branch
        branch_commits = get_commits_on_branch(base_api_url, expected_branch_unencoded, project_path_encoded)
        default_branch_commits = get_commits_on_branch(base_api_url, 'default', project_path_encoded)

        # Find the last commit that was on both branches.
        # Any commits made to our branch after this must have been made ONLY to our branch,
        # and not also to the default branch.
        last_shared_commit = None
        for branch_commit in branch_commits:
            if branch_commit in default_branch_commits:
                last_shared_commit = branch_commit
                break  # quit looking through commits

        # find all commits made to our branch AFTER it was split from the default branch
        branch_off_date = last_shared_commit['created_at']
        changes_url: str = f'{base_api_url}/projects/{project_path_encoded}/repository/commits?' \
                           f'ref_name={expected_branch_encoded}&' \
                           f'since={branch_off_date}'
        response, code = call_api(changes_url)
        branch_specific_commits = response.json()

        did_commit_edit_readme: bool = False
        for commit in branch_specific_commits:
            sha: str = commit['id']
            diffs = get_diffs_from_commit(base_api_url, project_path_encoded, sha)
            for diff in diffs:
                actual_edited_file: str = diff['old_path']
                if actual_edited_file == expected_edited_file:
                    did_commit_edit_readme = True
                    break  # quit looking at diffs in this commit
            if did_commit_edit_readme:
                break  # quit looking at this commit
        assert did_commit_edit_readme, f"didn't find any commits on the '{expected_branch_unencoded}' branch " \
                                       f"that edit the file '{expected_edited_file}'"
    else:
        # The branch doesn't exist, so was either never created or else
        # was deleted when the MR was merged. Look through MRs with "merged" state
        # to see if any have the expected branch name as their source branch.
        mrs_url: str = f'{base_api_url}/projects/{project_path_encoded}/merge_requests?' \
                       f'source_branch={expected_branch_encoded}'
        mrs_response, mrs_code = call_api(mrs_url)
        assert (mrs_code != 404), f"didn't find a branch called '{expected_branch_unencoded}' " \
                                  f"in the project at '{project_path_unencoded}'"

        # If we've gotten this far, we HAVE found MRs with the expected source branch. See if any
        # of them modified the expected edited file in their commits.
        mrs: list = mrs_response.json()
        found_file_in_commit: bool = False
        for mr in mrs:
            mr_iid: str = mr['iid']
            changes_url: str = f'{base_api_url}/projects/{project_path_encoded}/merge_requests/{mr_iid}/changes'
            changes_response, changes_code = call_api(changes_url)
            all_response_content: dict = changes_response.json()
            changes: list = all_response_content['changes']
            for change in changes:
                if change['old_path'] == expected_edited_file:
                    found_file_in_commit = True
                    break
        assert found_file_in_commit, f"didn't find any merge requests with branch '{expected_branch_unencoded}' " \
                                     f"as the source branch, that are in a 'merged' state, " \
                                     f"that include a commit that changes the file '{expected_edited_file}' " \
                                     f"in the project at '{project_path_unencoded}'"


def test_task_3() -> None:
    """Create a merge request for your README change and merge into your master branch."""
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    state: str = 'merged'
    expected_branch_unencoded: str = 'branch-update-readme'
    edited_file_name: str = 'README.md'

    url: str = f'{base_api_url}/projects/{project_path_encoded}/merge_requests?state={state}'
    response, code = call_api(url)
    assert (code != 404), f"the project at '{project_path_unencoded}' " \
                          f"doesn't contain any merge requests in '{state}' state"
    mrs: list = response.json()
    # Look through each merge request to see if any contain commits that change README.md
    found_file_in_commit: bool = False
    for mr in mrs:
        mr_source_branch: str = mr['source_branch']
        if mr_source_branch == expected_branch_unencoded:
            mr_iid: str = mr['iid']
            url: str = f'{base_api_url}/projects/{project_path_encoded}/merge_requests/{mr_iid}/changes'
            response, code = call_api(url)
            response_content_structured: dict = response.json()
            changes: list = response_content_structured['changes']
            for change in changes:
                if change['old_path'] == edited_file_name:
                    found_file_in_commit = True
                    break
    assert found_file_in_commit, f"didn't find any merge requests with branch '{expected_branch_unencoded}'" \
                                 f"as the source branch, that are in a 'merged' state, " \
                                 f"that include a commit that changes '{edited_file_name}' " \
                                 f"in the project at '{project_path_unencoded}'"


def test_task_4() -> None:
    """Create an issue titled 'new feature'."""
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    issue_title_unencoded: str = 'new feature'
    issue_title_encoded: str = urlencode(issue_title_unencoded)
    url: str = f'{base_api_url}/projects/{project_path_encoded}/issues?search={issue_title_encoded}&in=title'
    response, code = call_api(url)
    assert code != 404, f"didn't find any issues in the project at '{project_path_unencoded}'"
    issues: list = response.json()
    assert(len(issues) > 0), f"didn't find an issue called '{issue_title_unencoded}' " \
                             f"in the project at '{project_path_unencoded}'"


def test_task_5() -> None:
    """Add an "in production" label to the previous issue."""
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    issue_title: str = 'new feature'
    label: str = 'in production'
    url: str = f'{base_api_url}/projects/{project_path_encoded}/issues?search={issue_title}&in=title&labels={label}'
    response, code = call_api(url)
    assert (code != 404), f"didn't find an issue titled '{issue_title}' with the label '{label}' " \
                          f"in the project at '{project_path_unencoded}'"
    issues: list = response.json()
    assert (len(issues) > 0), f"didn't find an issue titled '{issue_title}' with the label '{label}' " \
                              f"in the project at '{project_path_unencoded}'"


def test_task_6() -> None:
    """Promote the issue to an epic. Note: do not simply create a new epic."""
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    issue_and_epic_title_unencoded: str = 'new feature'
    issue_and_epic_title_encoded: str = urlencode(issue_and_epic_title_unencoded)
    group_path: str = extract_group_path(project_path_encoded)
    url: str = f'{base_api_url}/groups/{group_path}/epics?search={issue_and_epic_title_encoded}&in=title'
    epics_response, code = call_api(url)
    assert (code != 404), f"didn't find any epics in group at '{group_path}'"

    epics: list = epics_response.json()
    assert (len(epics) > 0), f"didn't find any epics in group at '{group_path}'"

    # Sometimes users create multiple epics with the same name, so we need to check each one to see if it was promoted.
    was_promoted: bool = False
    for epic in epics:
        epic_id: str = epic['id']
        url: str = f'{base_api_url}/groups/{group_path}/epics/{epic_id}/notes'
        epic_notes_response, epic_notes_code = call_api(url)
        notes: list = epic_notes_response.json()
        for note in notes:
            body: str = note['body']
            if 'promoted' in body:
                was_promoted = True
                break
    assert was_promoted, f"didn't find an epic in the group at '{group_path}' " \
                         f"that was promoted from an issue titled '{issue_and_epic_title_unencoded}'"


def test_task_7() -> None:
    """Create a second issue under the epic and title it 'add instructions' and add a label to it titled 'to do'."""
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    epic_title_unencoded: str = 'new feature'
    expected_issue_title: str = 'add instructions'
    expected_issue_label: str = 'to do'

    group_path_encoded: str = extract_group_path(project_path_encoded)
    group_path_unencoded = urlunencode(group_path_encoded)
    matching_issues = find_issues_with_title_and_label_under_epic(base_api_url,
                                                                  epic_title_unencoded,
                                                                  group_path_encoded,
                                                                  expected_issue_title,
                                                                  expected_issue_label)

    assert (len(matching_issues) > 0), f"didn't find any issues titled '{expected_issue_title}' " \
                                       f"with the label '{expected_issue_label}' " \
                                       f"in an epic titled '{epic_title_unencoded}' " \
                                       f"in the group at '{group_path_unencoded}'"


def test_task_8() -> None:
    """Use a quick action to log 20 minutes of time spent to the second issue."""
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    epic_title_unencoded: str = 'new feature'
    group_path_encoded: str = extract_group_path(project_path_encoded)
    group_path_unencoded = urlunencode(group_path_encoded)
    expected_issue_title: str = 'add instructions'
    expected_issue_label: str = 'to do'
    matching_issues = find_issues_with_title_and_label_under_epic(base_api_url,
                                                                  epic_title_unencoded,
                                                                  group_path_encoded,
                                                                  expected_issue_title,
                                                                  expected_issue_label)
    expected_time_spent: str = '20m'
    found_issue_with_right_time_spent: bool = False
    for issue in matching_issues:
        time_stats: dict = issue['time_stats']
        actual_time_spent: str = time_stats['human_total_time_spent']
        if actual_time_spent == expected_time_spent:
            found_issue_with_right_time_spent = True
            break  # quit looking at issues
    assert found_issue_with_right_time_spent, f"didn't find an issue titled '{expected_issue_title}' " \
                                              f"with the label '{expected_issue_label}' " \
                                              f"with '{expected_time_spent}' time spent " \
                                              f"in an epic titled '{epic_title_unencoded}' " \
                                              f"in the group at '{group_path_unencoded}'"


def test_task_9() -> None:
    """
    Add a picture of your company logo to the root level of the project repository.
    The file name must be "logo.<extension>".
    For example: "logo.png" is OK, but "my-logo.png" is not.
    """
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    branches = ['master', 'main']
    file_paths: list = ['logo.jpg', 'logo.jpeg', 'logo.gif', 'logo.png', 'logo.svg', 'logo.bmp'
                        'logo.JPG', 'logo.JPEG', 'logo.GIF', 'logo.PNG', 'logo.SVG', 'logo.BMP']
    found_file: bool = False
    for branch in branches:
        for file_path_unencoded in file_paths:
            file_path_encoded = urlencode(file_path_unencoded)
            url: str = f'{base_api_url}/projects/{project_path_encoded}/' \
                       f'repository/files/{file_path_encoded}?ref={branch}'
            response, code = call_api(url)
            if code == 200:
                found_file = True
                break  # don't look for any other file paths
        if found_file:
            break  # don't look in any other branches
    assert found_file, f"didn't find a file with any of the names {file_paths} in any of the branches {branches}"


def test_task_10() -> None:
    """Create a snippet in your project."""
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    url: str = f'{base_api_url}/projects/{project_path_encoded}/snippets'
    response, code = call_api(url)
    snippets: list = response.json()
    err_msg: str = f"didn't find any snippets in the project at '{project_path_unencoded}'"
    assert code != 404, err_msg
    assert(len(snippets) > 0), err_msg


def test_task_11() -> None:
    """
    Create a Wiki Page titled "GitLab Certified Associate Wiki" with the following information
    from your project:
        * link to your issue board
        * link to your project repo
        * link to the epic from step 5
    Grader's note: Because there are a ton of ways students can make these links,
    Kendra suggests we just check that a Wiki page with the correct title exists.
    And since the GitLab UI doesn't let you save the page if it's empty, we don't
    even need to make sure it's non-empty.
    """
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    slug: str = 'GitLab Certified Associate Wiki'
    url: str = f'{base_api_url}/projects/{project_path_encoded}/wikis/{slug}'
    response, code = call_api(url)
    assert (code != 404), f"didn't find a Wiki page titled '{slug}' in the project at '{project_path_unencoded}'"


def test_task_12() -> None:
    """Fork your project."""
    base_api_url, project_path_unencoded, project_path_encoded = get_urls()
    url: str = f'{base_api_url}/projects/{project_path_encoded}/forks'
    response, code = call_api(url)
    forks: list = response.json()
    assert (len(forks) > 0), f"didn't find any forks of the project at '{project_path_unencoded}'"
