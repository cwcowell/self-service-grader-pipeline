# pip uses this file to determine which Python modules to install

pytest == 6.2.4
pytest-json-report == 1.3.0
requests == 2.25.1
